/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.conversion;

/**
 *
 * @author Luis
 */
public class DecimalAHexadecimal {
    
    public String numDecimal;

    public String convercionDecimalAHexadecimal(String numDecimal){
            String resultado = "";
            int decimal = Integer.parseInt(numDecimal);
            String[] numHexa = new String[numDecimal.length()+1];
            int residuo;
            int x = 0;
            do{
                residuo = decimal % 16;
                          decimal = decimal /16;                
                if(residuo > 9 && residuo < 16){
                    numHexa[x] = conversion(residuo+"");
                    x++;
                }else{
                    numHexa[x] = residuo+"";
                    x++;
                }                        
              }while(residuo != 0 && residuo != 1);               
             for(int y = numHexa.length-1; y >= 0; y--){            
                if(!(numHexa[y] == null)){
                  resultado += numHexa[y];
                }
        }
        return resultado;
    }
    
     public String conversion(String numHexadecimal){  
        int num = Integer.parseInt(numHexadecimal);     
        String letra ="";
        switch(num){
            case 10: letra = "A";break;
            case 11: letra = "B";break;
            case 12: letra = "C";break;
            case 13: letra = "D";break;
            case 14: letra = "E";break;    
            case 15: letra = "F";   
        }      
        return letra;
    }
    
    public String getNumDecimal() {
        return numDecimal;
    }
    public void setNumDecimal(String numDecimal) {
        this.numDecimal = numDecimal;
    }
    public static void main(String[] args){
        DecimalAHexadecimal convertidor = new DecimalAHexadecimal();
        convertidor.setNumDecimal("422");
        System.out.println("Convertidor de Número Decimal a Hexadecimal");
        System.out.println("Ejemplo del libro");
        System.out.println("Número decimal = "+convertidor.getNumDecimal()+" = "
                          +convertidor.convercionDecimalAHexadecimal(convertidor.getNumDecimal())+" Número Hexadecimal");
    }
}
