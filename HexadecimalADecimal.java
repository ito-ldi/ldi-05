/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.conversion;

/**
 *
 * @author Luis Silva
 */
public class HexadecimalADecimal{
    private String numHexadecimal;
   
    public String HexadecimalADecimal(String numHexadecimal){
        int suma = 0;
        double multi = 0;
        int exponente = 0;
        int numBi = 0;
        for(int x= numHexadecimal.length(); x > 0; x--){
          String numBinario = numHexadecimal.substring(x-1, x);  
          if(numBinario.equals("A")||numBinario.equals("B")||numBinario.equals("C")||numBinario.equals("D")||numBinario.equals("E")||numBinario.equals("F")){
                multi = Math.pow(16,exponente);                 
                suma += conversion(numBinario)*multi;
                exponente++;             
            }else{
               numBi = Integer.parseInt(numBinario);
               multi = Math.pow(16,exponente);
               suma += numBi * multi;
               exponente++; 
            }       
            }               
             return suma+"";
        }
    
    public int conversion(String numHexadecimal){  
        char[] letra = numHexadecimal.toCharArray();  
        int num = 0;     
        switch(letra[0]){
            case 'A': num = 10;break;
            case 'B': num = 11;break;
            case 'C': num = 12;break;
            case 'D': num = 13;break;
            case 'E': num = 14;break;    
            case 'F': num = 15;   
        }      
        return num;
    }
    
     public String getNumHexadecimal() {
        return numHexadecimal;
    }
    public void setNumHexadecimal(String numHexadecimal) {
        this.numHexadecimal = numHexadecimal;
    }    
    
    public static void main(String[] args){
        HexadecimalADecimal conversion = new HexadecimalADecimal();
        conversion.setNumHexadecimal("3BA4");
        
        System.out.println("Convertidor de Número Hexadecimal a Decimal");
        System.out.println("Ejemplo del libro");
        System.out.println("Número Hexadecimal = "+conversion.getNumHexadecimal()
                          +" = "+conversion.HexadecimalADecimal(conversion.getNumHexadecimal())+" Número Decimal");
    }
}
